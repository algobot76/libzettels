//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.

//! # Backstage
//
//! Just a private module serving as a viewscreen to hide away the inner 
//! and makes it possible to control visibility in the library root while 
//! everything within can see each other.

// --------------------------------------------------------------------------

pub mod configuration;
pub mod error;
pub mod index;
pub mod indexing;
pub mod querying;
pub mod zettel;