//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with libzettels. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------

extern crate libzettels;
extern crate tempfile;
use self::tempfile::tempdir;

use libzettels::{Config, Index, IndexingMethod};
use libzettels::examples::*;
use std::path::Path;
use std::io::Write;

const FILE6: &str ="---
title:  'File 6'
keywords: [example6]
followups: [file1.md]
lang: de
...

# Bla bla

Lorem [ipsum](file2.md)

[//]: # (Links)
";

#[test]
fn test_load_update_save_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_examples_with_index(dir).expect("Failed to generate examples");
    
    let config_dir = dir.join("examples/config");
    let cfg_file = config_dir.join("libzettels.cfg.yaml");
    let mut cfg = Config::from_file(cfg_file).expect("Failed to load config");
    cfg.indexingmethod = IndexingMethod::Grep;
    
    let mut index = Index::load(&cfg).expect("Failed to load index");
    
    let mut file6 = std::fs::File::create(&cfg.rootdir.join("file6.md"))
            .expect("Failed to create file6.md");
    writeln!(file6, "{}", FILE6).expect("Failed to write to file");
    
    let r = index.update(&cfg);
    assert!(r.is_ok());
    assert!(index.files.contains_key(Path::new("file6.md")));
    
    let z = index.get_zettel(Path::new("file6.md")).unwrap();
    assert_eq!(z.title, "File 6");
    assert_eq!(z.links.len(), 1);
    assert!(z.links.contains(&Path::new("file2.md").to_path_buf()));
    assert_eq!(z.keywords.len(), 1);
    assert!(z.keywords.contains(&"example6".to_string()));
    assert_eq!(z.followups.len(), 1);
    assert!(z.followups.contains(&Path::new("file1.md").to_path_buf()));
    
    let r = index.save(&cfg);
    assert!(r.is_ok());
}

#[test]
fn test_load_update_save_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_examples_with_index(dir).expect("Failed to generate examples");
    
    let config_dir = dir.join("examples/config");
    let cfg_file = config_dir.join("libzettels.cfg.yaml");
    let mut cfg = Config::from_file(cfg_file).expect("Failed to load config");
    cfg.indexingmethod = IndexingMethod::RipGrep;
    
    let mut index = Index::load(&cfg).expect("Failed to load index");
    
    let mut file6 = std::fs::File::create(&cfg.rootdir.join("file6.md"))
            .expect("Failed to create file6.md");
    writeln!(file6, "{}", FILE6).expect("Failed to write to file");
    
    let r = index.update(&cfg);
    assert!(r.is_ok());
    assert!(index.files.contains_key(Path::new("file6.md")));
    
    let z = index.get_zettel(Path::new("file6.md")).unwrap();
    assert_eq!(z.title, "File 6");
    assert_eq!(z.links.len(), 1);
    assert!(z.links.contains(&Path::new("file2.md").to_path_buf()));
    assert_eq!(z.keywords.len(), 1);
    assert!(z.keywords.contains(&"example6".to_string()));
    assert_eq!(z.followups.len(), 1);
    assert!(z.followups.contains(&Path::new("file1.md").to_path_buf()));
        
    let r = index.save(&cfg);
    assert!(r.is_ok());
}

#[test]
fn test_load_update_save_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_examples_with_index(dir).expect("Failed to generate examples");
    
    let config_dir = dir.join("examples/config");
    let cfg_file = config_dir.join("libzettels.cfg.yaml");
    let mut cfg = Config::from_file(cfg_file).expect("Failed to load config");
    cfg.indexingmethod = IndexingMethod::Native;
    
    let mut index = Index::load(&cfg).expect("Failed to load index");
    
    let mut file6 = std::fs::File::create(&cfg.rootdir.join("file6.md"))
            .expect("Failed to create file6.md");
    writeln!(file6, "{}", FILE6).expect("Failed to write to file");
    
    let r = index.update(&cfg);
    assert!(r.is_ok());
    assert!(index.files.contains_key(Path::new("file6.md")));
    
    let z = index.get_zettel(Path::new("file6.md")).unwrap();
    assert_eq!(z.title, "File 6");
    assert_eq!(z.links.len(), 1);
    assert!(z.links.contains(&Path::new("file2.md").to_path_buf()));
    assert_eq!(z.keywords.len(), 1);
    assert!(z.keywords.contains(&"example6".to_string()));
    assert_eq!(z.followups.len(), 1);
    assert!(z.followups.contains(&Path::new("file1.md").to_path_buf()));
        
    let r = index.save(&cfg);
    assert!(r.is_ok());
}