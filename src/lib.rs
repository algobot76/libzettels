//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/3505512/libzettels.png")]

//! Libzettels is a library intended as a backend for applications which 
//! implement Niklas Luhmann's system of a "Zettelkasten".
//! 
//! - If you want to develop a Zettelkasten application, and want to 
//!   know more about libzettel's approach, see the project's 
//!   README.
//! - If you (want to) develop a Zettelkasten application, and want to 
//!   have a look at libzettel's API, continue reading here.
//! - If you're looking for a Zettelkasten application, have a look at 
//!   [Zettels](https://gitlab.com/sthesing/zettels/).
//! - If you have no idea what a Zettelkasten is, have a look at 
//!   [Zettels' README](https://gitlab.com/sthesing/zettels/-/blob/master/README.md), 
//!   in particular the section "What the heck is a Zettelkasten?"
//!
//! # API Introduction for application developers
//! 
//! The heart of a Zettelkasten is the data representing the interrelations 
//! between the zettels. In libzettels, all that information is bundled in the 
//! [`Index`](struct.Index.html). See its [Fields](struct.Index.html#fields)  
//! for details.
//!
//! A YAML-representation of an example
//! Index might look like this:
//! ```yaml
//! ---    
//! files:
//!     file1.md:
//!         title: File 1
//!         followups: [file2.md]
//!         keywords: [example, first]
//!         links: [subdir/file3.md]
//!     file2.md:
//!         title: File 2
//!         followups: [subdir/file3.md]
//!         keywords: [example, second]
//!         links: []
//!     subdir/file3.md:
//!         title: File 3
//!         followups: []
//!         keywords: [example, third]
//!         links: [file1.md]
//! timestamp:
//!     secs_since_epoch: 1543078763
//!     nanos_since_epoch: 449322318
//! ```
//!
//! But where does that data come from? From the user's files that make up
//! the Zettelkasten. These files must contain the necessary information,
//! which is represented by the struct [`Zettel`](struct.Zettel.html).
//!
//! ## Two typical use cases, three central structs
//!
//! In essence, a user does two things with a Zettelkasten.
//!
//! 1. *Write* zettels and by doing so generate data about the interrelations
//!    between them.
//! 2. *Query* the Zettelkasten and inspect the interrelations between the 
//!    zettels.
//!
//! Roughly speaking, 
//!
//! 1. the [`Zettel`](struct.Zettel.html) struct (or rather the logic it 
//!    represents), is used for *writing*, while
//! 2. the [`Index`](struct.Index.html) is used for *querying* the 
//!    Zettelkasten. And finally,
//! 3. the [`Config`](struct.Config.html) struct represents the users
//!    configuration. You can easily extend this struct for your application.
//! 
//! So, depending on what aspect of your application you're working on, one
//! of these three is your place to start. But have a look at API Levels, 
//! first.
//!
//! ## API Levels
//!
//! libzettels discerns two API levels: **Basic** and **Extended**.
//! 
//! ### Which to choose
//!
//! libzettels was designed with a certain kind of applications in mind:
//! Applications, where users edit the YAML metadata of their zettels manually.
//! (If you have no idea what I'm talking about, see the section 
//! "What does libzettels do?" in the project's 
//! [README](https://gitlab.com/sthesing/libzettels/-/blob/master/README.md)).
//!
//! - If your application lets users **edit their zettel files** manually, 
//!   use the **Basic API**.
//! - If you want to **hide the zettel files** away and give them
//!   some GUI forms, rich text editing or the like, you'll probably need a  
//!   bit more granular control. That's what the **Extended API** is for.
//!
//! ### Don't worry
//! 
//! For simplicity's sake, the API levels are not hard-coded, enforced or
//! anything. They exist only in the documentation. 
//! Meaning: I have marked the
//! functions that you probably won't need with "Extended API". But feel free
//! to mix and match. I won't stop you.
// --------------------------------------------------------------------------
// Crates
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate log;
extern crate regex;
extern crate serde_yaml;
extern crate gitignore;

// Modules
mod backstage;
pub mod examples;

// Expose API
pub use backstage::configuration::Config;
pub use backstage::error::Error;
pub use backstage::index::Index;
pub use backstage::indexing::IndexingMethod;
pub use backstage::querying::sequences::SequenceStart;
pub use backstage::zettel::Zettel;