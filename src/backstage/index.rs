//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/3505512/libzettels.png")]

//use std::io::{BufRead, BufReader};
//use std::fmt::Write as FmtWrite; 
use std::path::{Path, PathBuf};
use std::collections::{BTreeMap, HashSet};
use std::fs;
use std::fs::File; 
use std::time::{SystemTime};

// From parent module
use backstage::configuration::Config;
use backstage::error::Error;
use backstage::indexing;
use backstage::querying::{self};
use backstage::querying::sequences::SequenceStart;
use backstage::zettel::Zettel;

/// The heart of a Zettelkasten is the data representing the interrelations 
/// between the zettels. All that information is bundled in the 
/// [`Index`](struct.Index.html). See its [Fields](struct.Index.html#fields)  
/// for details.
///
/// A YAML-representation of an example
/// Index might look like this:
/// ```yaml
/// ---    
/// files:
///     file1.md:
///         title: File 1
///         followups: [file2.md]
///         keywords: [example, first]
///         links: [subdir/file3.md]
///     file2.md:
///         title: File 2
///         followups: [subdir/file3.md]
///         keywords: [example, second]
///         links: []
///     subdir/file3.md:
///         title: File 3
///         followups: []
///         keywords: [example, third]
///         links: [file1.md]
/// timestamp:
///     secs_since_epoch: 1543078763
///     nanos_since_epoch: 449322318
/// ```
///
/// The Index provides the functionality necessary to query the Zettelkasten.
/// For that, it offers a
///
/// - Basic API, as well as an
/// - Extended API
/// 
/// Please refer to the 
/// [API Introduction](index.html#api-levels) for info on which to use.
///
/// # Basic API
/// ## Handling the Index itself
/// - [`new`](struct.Index.html#method.new)
/// - [`load`](struct.Index.html#method.load)  
/// - [`update`](struct.Index.html#method.update)
/// - [`save`](struct.Index.html#method.save)
///
/// ## Working with Zettels
/// - [`get_zettel`](struct.Index.html#method.get_zettel)|
///
/// ## Inspect Sequences
/// - [`sequences`](struct.Index.html#method.sequences)
/// - [`zettels_of_sequence`](struct.Index.html#method.zettels_of_sequence)
/// - [`sequence_tree`](struct.Index.html#method.sequence_tree)
/// - [`sequence_tree_whole`](struct.Index.html#method.sequence_tree_whole)
///
/// ## Inspect Links
/// - [`inspect_links`](struct.Index.html#method.inspect_links)
/// - [`inspect_incoming_links`](struct.Index.html#method.inspect_incoming_links)
///
/// ## Search
/// - [`search_keywords`](struct.Index.html#method.search_keywords)
/// - [`search_title`](struct.Index.html#method.search_title) 
/// - [`combi_search`](struct.Index.html#method.combi_search) 
///
/// ## Listing
/// - [`get_keywords`](struct.Index.html#method.get_keywords)
/// - [`count_keywords`](struct.Index.html#method.count_keywords)
///
/// # Extended API
/// All functions of the Basic API plus the following:
///
/// ## Handling the Index itself
///
/// - [`empty`](struct.Index.html#method.empty)
/// - [`from_yaml`](struct.Index.html#method.from_yaml)
/// - [`from_file`](struct.Index.html#method.from_file)
/// - [`update_timestamp`](struct.Index.html#method.update_timestamp)
/// ## Working with Zettels
/// - [`add_zettel`](struct.Index.html#method.add_zettel)
/// - [`remove_zettel`](struct.Index.html#method.remove_zettel)
/// - [`get_mut_zettel`](struct.Index.html#method.get_mut_zettel)
/// ## Inspect Sequences
/// - [`parents_of_zettel`](struct.Index.html#method.parents_of_zettel)
///
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Index {
    /// A key-value-store containing indexed data about the zettels.
    /// - The keys are paths to the respective zettel file (relative to the
    ///   root directory of the Zettelkasten).
    /// - The values are [`Zettel`s](struct.Zettel.html)
    pub files: BTreeMap<PathBuf, Zettel>,
    /// A timestamp that shows when the index was last updated.
    pub timestamp: SystemTime, 
}

impl Index {
    // ----------------------------------------------------------------------
    // Basic API
    // ----------------------------------------------------------------------
    
    /// Creates a new [`Index`](struct.Index.html) with data contained in
    /// [`Config`](struct.Config.html). The actual fields used are:
    /// - `rootdir`: path to the root directory containing the zettel files.
    /// - `indexingmethod`: the [`IndexingMethod`](enum.IndexingMethod.html) 
    ///   used.
    /// - `ignorefile`: path to the gitignore-compatible file telling the 
    ///   program which files in the root directory to ignore.
    ///
    /// **Note:** This really creates a *new* Index. All files in the root 
    /// directory are parsed. If you just want to load (and maybe update) an 
    /// existing index, use `Index::load` and `update` respectively.
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Config, Index};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::new(&cfg)?;
    /// # Ok(())
    /// # }
    /// ```
    /// **Note:**
    /// - If one of the files is non-text (e.g. an image) it is not added
    ///   to the index. An `info` about this is issued to logging.
    /// - If one of the files does not contain YAML-metadata, it is added to the 
    ///   index with the `title` "untitled" and empty `followups` and `keywords`. 
    ///   The markdown links (if any) are still parsed and `links` is populated 
    ///   as usual.
    /// # Errors
    /// - [`Error::BadLink`](enum.Error.html#variant.BadLink) if one of the 
    ///   files in the root directory  
    ///   links to a target that doesn't exist (both via `followups` and via
    ///   markdown link).
    /// - [`Error::IgnoreFile`](enum.Error.html#variant.IgnoreFile) for problems
    ///   applying an existing ignore file. (A missing ignore file is no problem).
    /// - [`Error::Io`](enum.Error.html#variant.Io) wrapping several kinds of 
    ///   `std::io:Error`, e.g. problems executing grep or ripgrep, problems with 
    ///   the files etc.
    /// - [`Error::NormalizePath`](enum.Error.html#variant.NormalizePath) if one of 
    ///   the files or a link in `followups` or `links`can not be expressed 
    ///   relative to the root directory
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) when deserializing a
    ///   Zettel from YAML failed for one of the files (but only if it contains
    ///   any YAML, at all).
    pub fn new(cfg: &Config) -> Result<Index, Error> {
        indexing::create_index(&cfg.indexingmethod,
                               &cfg.rootdir,
                               &cfg.ignorefile)                  //error::Error
    }
    
    /// Loads an existing [`Index`](struct.Index.html) from the file  
    /// specified in the `indexfile` field of [`Config`](struct.Config.html).
    ///
    /// **Note**: This only loads the information contained in the index file.
    /// Changes in the zettel files since last update of the index are not 
    /// represented in the index (i.e. since 
    /// [`Index::new()`](struct.Index.html#method.load) or 
    /// [`index.update()`](struct.Index.html#method.update) were called).
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Index, Config, Error};
    /// # fn foo() -> Result<(), Error>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// # Ok(())
    /// # }
    /// ```
    /// # Errors
    /// - [`Error::Io`](enum.Error.html#variant.Io) for problems reading the
    ///   index file specified in `Config`'s field `indexfile`.
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) when deserializing from
    ///   YAML failed.
    pub fn load(cfg: &Config) -> Result<Index, Error> {
        Index::from_file(&cfg.indexfile)
    }
    
    /// Updates an existing index using the info contained in the 
    /// [`Config`](struct.Config.html) passed as an argument. Only files 
    /// modified after the index' timestamp are inspected.
    /// 
    /// The actual `Config` fields used are:
    /// - `cfg.rootdir`
    /// - `cfg.ignorefile`
    /// - `cfg.indexingmethod`
    ///
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Index, Config, Error};
    /// # fn foo() -> Result<(), Error>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let mut index = Index::load(&cfg)?;
    /// index.update(&cfg)?;
    /// # Ok(())
    /// # }
    /// ```
    /// **Note:**
    /// - If one of the files is non-text (e.g. an image) it is not added
    ///   to the index. An `info` about this is issued to logging.
    /// - If one of the files does not contain YAML-metadata, it is added to the 
    ///   index with the `title` "untitled" and empty `followups` and `keywords`. 
    ///   The markdown links (if any) are still parsed and `links` is populated 
    ///   as usual.
    /// # Errors
    /// - [`Error::BadLink`](enum.Error.html#variant.BadLink) if one of the files 
    ///   links to a target that doesn't exist (both via `followups` and via
    ///   markdown link).
    /// - [`Error::IgnoreFile`](enum.Error.html#variant.IgnoreFile) for problems
    ///   applying an existing ignore file. (A missing ignore file is no problem).
    /// - [`Error::Io`](enum.Error.html#variant.Io) wrapping several kinds of 
    ///   `std::io:Error`, e.g. problems executing grep or ripgrep, problems with 
    ///   the files etc.
    /// - [`Error::NormalizePath`](enum.Error.html#variant.NormalizePath) if one of 
    ///   the files or a link in `followups` or `links`can not be expressed 
    ///   relative to the root directory
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) when deserializing a
    ///   Zettel from YAML failed for one of the files (but only if it contains
    ///   any YAML, at all).
    pub fn update(&mut self, cfg: &Config) -> Result<(), Error> {
        indexing::update_index(self,
                               &cfg.indexingmethod, 
                               &cfg.rootdir,
                               &cfg.ignorefile)                  //error::Error
    }

    /// Write a YAML-representation to the file specified in the `Config` field
    /// `indexfile`.
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// index.save(&cfg)
    ///     .expect("Failed to save index to file.");
    /// # Ok(())
    /// # }
    /// ```
    /// # Errors
    /// - [`Error::Io`](enum.Error.html#variant.Io) for problems with the 
    ///   specified indexfile.
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) for problems 
    ///   serializing to YAML.
    pub fn save(&self, cfg: &Config) -> Result<(), Error> {
        self.to_file(&cfg.indexfile)
    }
  
    /// Returns a reference to the [Zettel](struct.Zettel.html) found at `key`. 
    /// Returns `None` if said key is not present.
    /// The `key` is a path to the file relative to root directory. 
    ///
    /// Internally, this function calls the method
    /// [`get`](https://doc.rust-lang.org/nightly/alloc/collections/btree_map/struct.BTreeMap.html#method.get) 
    ///  of [`std::collections::BTreeMap`](https://doc.rust-lang.org/nightly/alloc/collections/btree/map/struct.BTreeMap.html). 
    /// See documentation there for details.
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Index, Zettel, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let z1 = index.get_zettel(Path::new("file1.md"));
    /// assert!(z1.is_some());
    /// let z1 = z1.unwrap();
    /// assert_eq!(z1.title, "File 1");
    /// # Ok(())
    /// # }
    /// ```
    pub fn get_zettel<P: AsRef<Path>>(&self, key: P) -> Option<&Zettel> {
        self.files.get(key.as_ref())
    }
  
    /// Returns a list of all the sequences the Zettels specified by `scope` is
    /// a part of.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let mut scope = HashSet::new();
    /// scope.insert(PathBuf::from("file1.md"));
    /// let sequences = index.sequences(&scope, &cfg.sequencestart);
    /// # Ok(())
    /// # }
    /// ```
    pub fn sequences(&self,
                              scope: &HashSet<PathBuf>,
                              cfg_sequence_start: &SequenceStart)
                              -> HashSet<PathBuf> {
        querying::sequences::sequences(&self, scope, cfg_sequence_start)
    }
    
    /// Using the Zettel specified by `key` as an identifier for a sequence, this 
    /// function returns a list of all the zettels that belong to that sequence.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let seq_zettels = index.zettels_of_sequence(PathBuf::from("file1.md"));
    /// # Ok(())
    /// # }
    /// ```
    pub fn zettels_of_sequence<P: AsRef<Path>>(&self, key: P)
                                                   -> HashSet<PathBuf> {
        querying::sequences::zettels_of_sequence(&self, key)
    }
    
    /// Returns a list of zettels that are part of all sequences of which the 
    /// zettels specified by `scope` are also a part of.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let mut scope = HashSet::new();
    /// scope.insert(PathBuf::from("file1.md"));
    /// let sequence_tree = index.sequence_tree(&scope);
    /// assert!(sequence_tree.contains(&PathBuf::from("file1.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("file2.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("file3.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("subdir/file4.md")));
    /// # Ok(())
    /// # }
    /// ```
    pub fn sequence_tree(&self, scope: &HashSet<PathBuf>) -> HashSet<PathBuf> {
        querying::sequences::sequence_tree(&self, &scope)
    }
    
    /// Returns a list of zettels that are part of all sequences of which the 
    /// zettels specified by `scope` are also a part of, as well as related 
    /// sequences.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let mut scope = HashSet::new();
    /// scope.insert(PathBuf::from("file1.md"));
    /// let sequence_tree = index.sequence_tree_whole(&scope);
    /// assert!(sequence_tree.contains(&PathBuf::from("file1.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("file2.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("file3.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("subdir/file4.md")));
    /// assert!(sequence_tree.contains(&PathBuf::from("subdir/file5.md")));
    /// # Ok(())
    /// # }
    /// ```
    pub fn sequence_tree_whole(&self, scope: &HashSet<PathBuf>)
                                        -> HashSet<PathBuf> {
        querying::sequences::sequence_tree_whole(&self, scope)
    }
    
    /// Collects the outgoing links of a list of zettels specified by
    /// `linkers`.
    /// Returns a `HashSet`.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// 
    /// // We want the links of one Zettel: file1.md
    /// let mut linkers = HashSet::new();
    /// linkers.insert(PathBuf::from("file1.md"));
    /// let links = index.inspect_links(&linkers);
    /// # Ok(())
    /// # }
    /// ```
    pub fn inspect_links(&self, scope: &HashSet<PathBuf>) 
                                                    -> HashSet<PathBuf> {
        let mut links = HashSet::new();
        for linker in scope {
            let z = &self.get_zettel(linker);
            if z.is_some() {
                for link in &z.unwrap().links {
                    links.insert(link.to_path_buf());
                }
            }
        }
        links
    }
    
    // wrapper for `querying::inspect_incoming`, unit tests are there.
    /// Returns a list of zettels that link to the zettels specified by
    /// `zettels`. If `all` is set to true, only zettels are returned that
    /// link to *all* specified zettels.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// 
    /// // We want zettels linking to two Zettels: file2.md and file3.md
    /// let mut targets = HashSet::new();
    /// targets.insert(PathBuf::from("file2.md"));
    /// targets.insert(PathBuf::from("file3.md"));
    ///
    /// // What links to either of them?
    /// let incoming = index.inspect_incoming_links(&targets, false);
    /// assert_eq!(incoming.len(), 3);
    /// assert!(incoming.contains(&PathBuf::from("file1.md")));
    /// assert!(incoming.contains(&PathBuf::from("file2.md")));
    /// assert!(incoming.contains(&PathBuf::from("onlies/markdown-only.md")));
    ///
    /// // What links to both of them?
    /// let incoming = index.inspect_incoming_links(&targets, true);
    /// assert_eq!(incoming.len(), 1);
    /// assert!(incoming.contains(&PathBuf::from("file1.md")));
    /// # Ok(())
    /// # }
    /// ```
    pub fn inspect_incoming_links(&self, scope: &HashSet<PathBuf>, all: bool)
                -> HashSet<PathBuf> {
        querying::inspect_incoming(&self, &scope, all)
    }
        
    // wrapper for `querying::search_index_for_keywords`
    /// Takes a vector of Strings, containing keywords to be searched for.
    /// Returns a list of zettels that have one or – if `all` is true – all
    /// of these keywords.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let searched_keywords = vec!["first".to_string()];
    /// let matching_zettels = index.search_keywords(searched_keywords,
    ///                                              false);
    /// assert_eq!(matching_zettels.len(), 1);
    /// assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
    /// # Ok(())
    /// # }
    /// ```
    pub fn search_keywords(&self, searched_keywords: Vec<String>,
              all: bool) -> HashSet<PathBuf> {
        querying::search_index_for_keywords(&self, searched_keywords, all)
    }
    
    //wrapper for `querying::search_index_for_title`
    /// Searches the index for zettels whose title matches `search_term` 
    /// (exactly if `exact` is true). 
    /// Returns a list of matching zettels.
        /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let matching_zettels = index.search_title("File 1", false);
    /// assert_eq!(matching_zettels.len(), 1);
    /// assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
    /// # Ok(())
    /// # }
    /// ```
    pub fn search_title<T: AsRef<str>>(&self, 
                                       search_term: T,
                                       exact: bool) 
                                       -> HashSet<PathBuf> {
        querying::search_index_for_title(&self, search_term, exact)
    }
    
    //wrapper for `querying::combi_search`
    /// Combines searches for keywords and title. 
    /// If `exact` is true, the title must match `search_term` exactly.
    /// If `all` is true, only zettels are returned that match all search
    /// criteria.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let searched_keywords = vec!["first".to_string()];
    /// let matching_zettels = index.combi_search(searched_keywords, 
    ///                                              true,
    ///                                              "File",
    ///                                              false);
    /// assert_eq!(matching_zettels.len(), 1);
    /// assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
    /// # Ok(())
    /// # }
    /// ```
    pub fn combi_search<T: AsRef<str>>(&self, 
                                       searched_keywords: Vec<String>,
                                       all: bool, 
                                       search_term: T,
                                       exact: bool) 
                                       -> HashSet<PathBuf> {
        querying::combi_search(&self, searched_keywords, all, 
                               search_term, exact)
    }
    
    //wrapper for `querying::get_keywords`
    /// Lists all keywords present in the zettels.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::HashSet;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<dyn std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let all_keywords = index.get_keywords();
    /// assert!(all_keywords.contains("test"));
    /// assert!(all_keywords.contains("example"));
    /// # Ok(())
    /// # }
    /// ```    
    pub fn get_keywords(&self) -> HashSet<String> {
        querying::get_keywords(&self)
    }
    
    //wrapper for `querying::count_keywords`
    /// Lists all keywords present in the zettels along with how often
    /// they occur in the whole zettelkasen.
    /// # Example
    /// ```rust
    /// # use std::path::{Path, PathBuf};
    /// # use std::collections::BTreeMap;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<dyn std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let keyword_count = index.count_keywords();
    /// assert_eq!(Some(&5), keyword_count.get("example"));
    /// assert_eq!(Some(&1), keyword_count.get("test"));
    /// # Ok(())
    /// # }
    /// ```    
    pub fn count_keywords(&self) -> BTreeMap<String, u32> {
        querying::count_keywords(&self)
    }
    
    
    // ----------------------------------------------------------------------
    // Extended API
    // ----------------------------------------------------------------------
    
    /// ## Extended API
    /// Creates a new Index with an empty file list and the current system
    /// time as timestamp.
    /// # Example - And a very elabourated one, at that.
    /// ```rust
    /// # use libzettels::Index;
    /// let index = Index::empty();
    /// ```
    pub fn empty() -> Index {
        Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        }
    }
    
    /// ## Extended API
    /// Creates a new Index by deserializing it from a YAML-string. Returns
    /// an error if something went wrong with deserializing it.
    /// # Example
    /// ```rust
    /// # use libzettels::{Index};
    /// # fn foo() -> Result<Index, Box<std::error::Error>>  {
    /// let yaml = "---    
    /// files:
    ///   file1.md:
    ///     title: File 1
    ///     followups: [subdir/file2.md]
    ///     keywords: [example]
    ///     links: []
    ///   subdir/file2.md:
    ///     title: File 2
    ///     followups: []
    ///     keywords: [example, second]
    ///     links: [file1.md]
    /// timestamp:
    ///     secs_since_epoch: 1543078763
    ///     nanos_since_epoch: 449322318";
    /// let index = Index::from_yaml(yaml)?;
    /// # Ok(index)
    /// # }    
    /// ```
    /// # Errors
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) when deserializing from
    ///   YAML failed.
    pub fn from_yaml<T: AsRef<str>>(yaml: T) -> Result<Index, Error> {
        let index: Index = serde_yaml::from_str(yaml.as_ref())?; // serde_yaml::Error
        Ok(index)
    }
    
    /// ## Extended API
    /// Loads an existing Index by deserializing it from a YAML-file. Returns
    /// an error if something went wrong with reading the file or deserializing
    /// it.
    /// ```rust, no_run
    /// # use libzettels::{Index};
    /// # use std::path::Path;
    /// # fn foo() -> Result<Index, Box<std::error::Error>>  {
    /// let file = Path::new("examples/index.yaml");
    /// let index = Index::from_file(file)?;
    /// # Ok(index)
    /// # }     
    /// ```
    /// # Errors
    /// - [`Error::Io`](enum.Error.html#variant.Io) for problems reading the
    ///   file.
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) when deserializing from
    ///   YAML failed.
    pub fn from_file<P: AsRef<Path>>(indexfile: P) -> Result<Index, Error> {
        let indexfile = indexfile.as_ref();
        debug!("Loading index from file: {:?}", indexfile);
        let yaml = fs::read_to_string(indexfile)?;          //io::Error
        Index::from_yaml(yaml)                             //error::Error::Yaml
    }
    
    /// ## Extended API
    /// Overwrites the timestamp with the current system time.
    /// # Example
    /// ```rust, no_run
    /// # use libzettels::{Config, Index};
    /// # use std::path::Path;
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let mut index = Index::new(&cfg)?;
    /// index.update_timestamp();
    /// # Ok(())
    /// # }
    /// ```
    pub fn update_timestamp(&mut self) {
        self.timestamp = SystemTime::now();
    }
    
    /// ## Extended API
    /// Write a YAML-representation of the index to file.
    /// # Example
    /// ```rust
    /// # use std::path::Path;
    /// # use libzettels::{Index, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// index.to_file(Path::new("examples/index.yaml"))?;
    /// # Ok(())
    /// # }
    /// ```
    /// # Errors
    /// - [`Error::Io`](enum.Error.html#variant.Io) for problems with the 
    ///   specified file.
    /// - [`Error::Yaml`](enum.Error.html#variant.Yaml) for problems 
    ///   serializing to YAML.
    pub fn to_file<P: AsRef<Path>>(&self, indexfile: P) -> Result<(), Error> {
        use std::io::Write; 
        let indexfile = indexfile.as_ref();
        debug!("Writing index to file {:?}", indexfile);
        let s = serde_yaml::to_string(self)?;
        let mut file = File::create(indexfile)?;
        writeln!(file, "{}", s)?;
        Ok(())
    }
    
    /// ## Extended API
    /// Adds a Zettel to the index with the path to the file relative to root 
    /// directory as `key`. Internally,
    /// [`insert`](https://doc.rust-lang.org/nightly/alloc/collections/btree_map/struct.BTreeMap.html#method.insert)
    ///  of [`std::collections::BTreeMap`](https://doc.rust-lang.org/nightly/alloc/collections/btree/map/struct.BTreeMap.html)
    /// is called. See documentation there for details. That method's return 
    /// value is disregarded, however.
    /// ```rust, no_run
    /// # use libzettels::{Config, Index, Zettel};
    /// # use std::path::Path;
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let mut index = Index::new(&cfg)?;
    /// let file = Path::new("test-zettel.md"); //relative to the root directory
    /// let zettel = Zettel::new("Empty Zettel for Testing");
    /// index.add_zettel(file, zettel);
    /// # Ok(())
    /// # }
    /// ```
    pub fn add_zettel<P: AsRef<Path>>(&mut self, key: P, zettel: Zettel) {
        self.files.insert(key.as_ref().to_path_buf(), zettel);
    }
    
    /// ## Extended API
    /// Removes a Zettel from the index. The `key` is the path to the 
    /// corresponding file, relative to the root directory. 
    /// 
    /// Returns the old Zettel found at the key or `None` if it was not present
    /// in the first place. Please note that the return value doesn't need to
    /// be checked for success. The entry for `key` is guaranteed to be gone,
    /// regardless of the return value.
    ///
    /// Internally,
    /// [`remove`](https://doc.rust-lang.org/nightly/alloc/collections/btree_map/struct.BTreeMap.html#method.remove)
    ///  of [`std::collections::BTreeMap`](https://doc.rust-lang.org/nightly/alloc/collections/btree/map/struct.BTreeMap.html)
    /// is called. See documentation there for details.
    /// # Example 1
    /// In most cases, you'll just disregard the return value.
    /// ```rust, no_run
    /// # use libzettels::{Config, Index, Zettel};
    /// # use std::path::Path;
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let mut index = Index::new(&cfg)?;
    /// let file = Path::new("test-zettel.md"); //relative to root directory
    /// let zettel = Zettel::new("Empty Zettel for Testing");
    /// index.add_zettel(file, zettel);
    /// index.remove_zettel(file);
    /// // or, if you want to be very tidy:
    /// let _ = index.remove_zettel(file);
    /// # Ok(())
    /// # }
    /// ```
    /// # Example 2
    /// If you want to do something with the old Zettel:
    /// ```rust, no_run
    /// # use libzettels::{Config, Index, Zettel};
    /// # use std::path::Path;
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// // snip
    /// # let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// # let mut index = Index::new(&cfg)?;
    /// # let file = Path::new("test-zettel.md"); //relative to root directory
    /// let zettel = Zettel::new("Empty Zettel for Testing");
    /// // We clone here so we have something to compare.
    /// index.add_zettel(file, zettel.clone());  
    /// let removed = index.remove_zettel(file);
    /// assert_eq!(removed, Some(zettel));
    /// # Ok(())
    /// # }
    /// ```
    pub fn remove_zettel<P: AsRef<Path>>(&mut self, key: P) -> Option<Zettel> {
        self.files.remove(key.as_ref())
    }
    
    /// ## Extended API
    /// Returns a mutable reference to the [Zettel](struct.Zettel.html) found 
    /// at `key`. 
    /// Returns `None` of said key is not present.
    /// The `key` is the path to the corresponding file relative to root 
    /// directory. 
    /// 
    /// **Note:** Editing this only changes the entry in the index, not the 
    /// file itself. Changes might be overwritten when the index us updated
    /// next. This method is intended for developers of frontends who want to
    /// present the metadata and the document body to their users, separately. 
    /// If you use this, take care to write changes back to the file.
    ///
    /// Internally, this function calls the method
    /// [`get_mut`](https://doc.rust-lang.org/nightly/alloc/collections/btree_map/struct.BTreeMap.html#method.get_mut) 
    ///  of [`std::collections::BTreeMap`](https://doc.rust-lang.org/nightly/alloc/collections/btree/map/struct.BTreeMap.html). 
    /// See documentation there for details.
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Index, Zettel, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let mut index = Index::load(&cfg)?;
    /// // Use an inner scope so z goes out of scope automatically.
    /// {
    ///     let z = index.get_mut_zettel(Path::new("file1.md"));
    ///     assert!(z.is_some());
    ///     let z = z.unwrap();
    ///     z.title = String::from("Changed Title 1");
    /// }
    /// // Let's get it again from index to see if the changes are there.
    /// let z = index.get_zettel(Path::new("file1.md")).unwrap();
    /// assert_eq!(z.title, "Changed Title 1");
    /// # Ok(())
    /// # }
    /// ```
    pub fn get_mut_zettel<P: AsRef<Path>>(&mut self, key: P) -> Option<&mut Zettel> {
        self.files.get_mut(key.as_ref())
    }
    
    /// ## Extended API
    /// Returns the paths to all zettels in the index that list the zettel
    /// specified by `key` as their followups.
    /// # Note
    /// In Luhmann's Zettelkasten system, a Zettel can not be a followup of 
    /// more than one other Zettel. A user might approach this differently,
    /// however, so this returns a list (a HashSet, to be exact).
    /// # Example
    /// ```rust, no_run
    /// # use std::path::Path;
    /// # use libzettels::{Index, Zettel, Config};
    /// # fn foo() -> Result<(), Box<std::error::Error>>  {
    /// let cfg = Config::from_file(Path::new("examples/zettels-examples.cfg.yaml"))?;
    /// let index = Index::load(&cfg)?;
    /// let parents = index.parents_of_zettel("file2.md");
    /// for parent in parents {
    ///     println!("{:?}", parent);
    /// }
    /// # Ok(())
    /// # }
    /// ```
    pub fn parents_of_zettel<P: AsRef<Path>>(&self, key: P) 
                                -> HashSet<PathBuf> {
        let scope = vec![key.as_ref().to_path_buf()];
        querying::sequences::parents_of_scope(&self, scope)
    }

}

// ---------------------------------------------------------------------------
// Tests
// ---------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;
    use IndexingMethod;
    use std::io::Write;
    
    extern crate tempfile;
    use self::tempfile::tempdir;
    use examples::*;
    const YAML: &str = "---    
files:
  file1.md:
    title: File 1
    followups: [subdir/file2.md]
    keywords: [example]
    links: []
  subdir/file2.md:
    title: File 2
    followups: []
    keywords: [example, second]
    links: [file1.md]
timestamp:
    secs_since_epoch: 1543078763
    nanos_since_epoch: 449322318";
    
    // ----------------------------------------------------------------------
    // with valid data
    // ----------------------------------------------------------------------
    
    #[test]
    fn test_index_new() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let mut cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        // With default grep
        let index = Index::new(&cfg);
        cfg.indexingmethod = IndexingMethod::Grep;
        assert!(index.is_ok());
        let index_grep = index.unwrap();
        
        // with ripgrep
        cfg.indexingmethod = IndexingMethod::RipGrep;
        let index = Index::new(&cfg);
        assert!(index.is_ok());
        let index_rg = index.unwrap();
        
        // with native
        cfg.indexingmethod = IndexingMethod::Native;
        let index = Index::new(&cfg);
        assert!(index.is_ok());
        let index_native = index.unwrap();
        
        // All three indexes should be identical, except for the timestamp
        assert_eq!(index_grep.files, index_rg.files);
        assert_eq!(index_grep.files, index_native.files);
    }
    
    #[test]
    fn test_index_load() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        let index = Index::load(&cfg);
        assert!(index.is_ok());
    }
    
    #[test]
    fn test_index_update() {
        use std::io::Write;
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        let mut index = Index::load(&cfg)
            .expect("Failed to read index from file.");
            
        let mut file6 = std::fs::File::create(&cfg.rootdir.join("file6.md"))
            .expect("Failed to create file6.md");
        writeln!(file6, "{}", FILE6)
            .expect("Failed to write to file"); //see below
        
        assert!(index.update(&cfg).is_ok());
        assert!(index.files.contains_key(std::path::Path::new("file6.md")));
    }

    const FILE6: &str ="---
    title:  'File 6'
    keywords: [example6]
    followups: [file1.md]
    lang: de
    ...

    # Bla bla

    Lorem ipsum

    [//]: # (Links)
    ";
    
    #[test]
    fn test_index_empty() {
        let i1 = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let i2 = Index::empty();
        assert!(i2.files.is_empty());
        assert_eq!(i2.files, i1.files);
        // has i1 been created before i2?
        assert!(i1.timestamp < i2.timestamp);
    }

    #[test]
    fn test_index_from_yaml() {
        let index = Index::from_yaml(YAML);
        assert!(index.is_ok());
    }
    
    #[test]
    fn test_index_from_file() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let file_path = dir.join("examples/config/index.yaml");
        
        let index = Index::from_file(&file_path);
        assert!(index.is_ok());
    }
        
    #[test]
    fn test_index_add_zettel() {
        let mut index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let zettel = Zettel::new("Test Zettel");
        let zettel_copy = zettel.clone();
        index.add_zettel(&PathBuf::from("test_zettel.md"), zettel);
        assert_eq!(index.files.len(), 1);
        assert_eq!(index.files.get(&PathBuf::from("test_zettel.md")).unwrap(), 
                   &zettel_copy);
    }
    
    #[test]
    fn test_index_get_zettel() {
        let mut index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let zettel = Zettel::new("Test Zettel");
        let zettel_copy = zettel.clone();
        index.files.insert(PathBuf::from("test_zettel.md"), zettel);
        assert_eq!(index.get_zettel(PathBuf::from("test_zettel.md")).unwrap(), 
                   &zettel_copy);
    }
    
    #[test]
    fn test_index_save() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        let index = Index::load(&cfg)
            .expect("Failed to read index from file.");
        assert!(index.save(&cfg).is_ok());
    }
    
    #[test]
    fn test_index_remove_zettel() {
        let mut index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let zettel = Zettel::new("Test Zettel");
        let key = PathBuf::from("test_zettel.md");
        index.files.insert(key.clone(), zettel);
        assert_eq!(index.files.len(), 1);
        index.remove_zettel(key);
        assert!(index.files.is_empty());
    }
    
    #[test]
    fn test_index_update_timestamp() {
        let mut index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let t1 = index.timestamp;
        index.update_timestamp();
        // timestamp later than t1?
        assert!(t1 < index.timestamp);
    }
    
    #[test]
    fn test_index_get_mut_zettel() {
        let mut index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let zettel = Zettel::new("Test Zettel");
        let key = PathBuf::from("test_zettel.md");
        index.files.insert(key.clone(), zettel);
        // Use an inner scope so z goes out of scope automatically.
        {
            let z = index.get_mut_zettel(&key);
            assert!(z.is_some());
            let z = z.unwrap();
            z.title = String::from("Changed Title 1");
        }
        // Let's get it again from index to see if the changes are there.
        let z = index.get_zettel(&key).unwrap();
        assert_eq!(z.title, "Changed Title 1");
    }
    
    #[test]
    fn test_index_to_file() {
        // Setup
        let tmp_dir = tempdir().expect("Failed to create temp dir.");
        let indexfile = tmp_dir.path().join("index.yaml");
        // Create an empty index
        let mut index = Index::empty();
        // Add two Zettels
        let z1 = Zettel::new("Zettel 1");
        let z2 = Zettel::new("Zettel 2");
        index.add_zettel(Path::new("foo"), z1);
        index.add_zettel(Path::new("bar"), z2);
        
        // write the index to file
        assert!(index.to_file(indexfile).is_ok());
    }
    
    // -----------------------------------------------------------------------
    // invalid data aka error handling
    // -----------------------------------------------------------------------
    
    #[test]
    fn test_index_new_no_rootdir() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let _rootdir = dir.join("Zettelkasten/");
        let indexfile = config_dir.join("index.yaml");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let cfg = Config::new(dir.join("foo"), //Non existing rootdir
                              indexfile);
        let index = Index::new(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Io(inner) => {
                match inner.kind() {
                    std::io::ErrorKind::NotFound => {
                        assert!(inner.to_string().contains("No such file or \
                        directory"));
                    },
                    _ => panic!("Expected a NotFound error, got: {:#?}", inner)
                }
            },
            _ => panic!("Expected a Io error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_load_non_existing_file() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let rootdir = dir.join("Zettelkasten/");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let indexfile = config_dir.join("foo"); //doesn't exist
        
        let cfg = Config::new(rootdir,
                              indexfile);
        
        let index = Index::load(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
            match e {
            Error::Io(inner) => {
                match inner.kind() {
                    std::io::ErrorKind::NotFound => {
                        assert!(inner.to_string().contains("No such file or \
                        directory"));
                    },
                    _ => panic!("Expected a NotFound error, got: {:#?}", inner)
                }
            },
            _ => panic!("Expected a Io error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_load_missing_field() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let rootdir = dir.join("Zettelkasten/");
        let indexfile = config_dir.join("index.yaml");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let cfg = Config::new(rootdir, indexfile.to_path_buf());

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---    
    file:
        file1.md:
            title: File 1
            followups: [file2.md]
            keywords: [example, first]
            links: [subdir/file3.md]
    timestamp:
        secs_since_epoch: 1543078763
        nanos_since_epoch: 449322318";
        writeln!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::load(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("missing field `files`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_load_duplicate_field() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let rootdir = dir.join("Zettelkasten/");
        let indexfile = config_dir.join("index.yaml");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let cfg = Config::new(rootdir, indexfile.to_path_buf());

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
    files:
        file1.md:
            title: File 1
            followups: [file2.md]
            keywords: [example, first]
            links: [subdir/file3.md]
    files: foo
    timestamp:
        secs_since_epoch: 1543078763
        nanos_since_epoch: 449322318";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::load(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("duplicate field `files`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_load_invalid_type() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let rootdir = dir.join("Zettelkasten/");
        let indexfile = config_dir.join("index.yaml");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let cfg = Config::new(rootdir, indexfile.to_path_buf());

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
    files: foo
    timestamp:
        secs_since_epoch: 1543078763
        nanos_since_epoch: 449322318";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::load(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("invalid type"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
        
    #[test]
    fn test_index_load_unknown_field() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let rootdir = dir.join("Zettelkasten/");
        let indexfile = config_dir.join("index.yaml");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let cfg = Config::new(rootdir, indexfile.to_path_buf());

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
    files:
        file1.md:
            title: File 1
            followups: [file2.md]
            keywords: [example, first]
            links: [subdir/file3.md]
    timestamp:
        secs_since_epoc: 1543078763
        nanos_since_epoch: 449322318";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::load(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("unknown field"));
                assert!(message.contains("expected `secs_since_epoch`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_load_invalid_value() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_config(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let rootdir = dir.join("Zettelkasten/");
        let indexfile = config_dir.join("index.yaml");
        let _cfg_file = config_dir.join("libzettels.cfg.yaml");
        
        let cfg = Config::new(rootdir, indexfile.to_path_buf());

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
    files:
        file1.md:
            title: File 1
            followups: [file2.md]
            keywords: [example, first]
            links: [subdir/file3.md]
    timestamp:
        secs_since_epoch: 1543078763
        nanos_since_epoch: 44932231800000000";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::load(&cfg);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("invalid value"));
                assert!(message.contains("expected u32"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_update_bad_yaml() {
        use std::io::Write;
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        let mut index = Index::load(&cfg)
            .expect("Failed to read index from file.");
            
        let mut file6 = std::fs::File::create(&cfg.rootdir.join("file6.md"))
            .expect("Failed to create file6.md");
        
        let erroneous_file = "---
    title:  'File 6'
    title: Foo
    ";
        writeln!(file6, "{}", erroneous_file)
            .expect("Failed to write to file"); //see below
        
        let result = index.update(&cfg);
        assert!(result.is_err());
        let e = result.unwrap_err();
        
        match e {
            Error::BadHeader(_, inner) => {
                let message = inner.to_string();
                assert!(message.contains("duplicate field"));
            },
            _ => panic!("Expected a BadHeader error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_update_one_file_only_grep() {
        use std::io::Write;
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        let mut index = Index::load(&cfg)
            .expect("Failed to read index from file.");
            
        let mut file7 = std::fs::File::create(&cfg.rootdir.join("file7.md"))
            .expect("Failed to create file7.md");
        writeln!(file7, "{}", FILE7)
            .expect("Failed to write to file"); //see below
        
        let r = index.update(&cfg);
        assert!(r.is_ok());
        assert!(index.files.contains_key(std::path::Path::new("file7.md")));
    }
    
    #[test]
    fn test_index_update_one_file_only_ripgrep() {
        use std::io::Write;
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let mut cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        cfg.indexingmethod = IndexingMethod::RipGrep;
        
        let mut index = Index::load(&cfg)
            .expect("Failed to read index from file.");
            
        let mut file7 = std::fs::File::create(&cfg.rootdir.join("file7.md"))
            .expect("Failed to create file7.md");
        writeln!(file7, "{}", FILE7)
            .expect("Failed to write to file"); //see below
        
        let r = index.update(&cfg);
        assert!(r.is_ok());
        assert!(index.files.contains_key(std::path::Path::new("file7.md")));
    }
    
    #[test]
    fn test_index_update_one_file_only_native() {
        use std::io::Write;
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let mut cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        cfg.indexingmethod = IndexingMethod::Native;
        
        let mut index = Index::load(&cfg)
            .expect("Failed to read index from file.");
            
        let mut file7 = std::fs::File::create(&cfg.rootdir.join("file7.md"))
            .expect("Failed to create file7.md");
        writeln!(file7, "{}", FILE7)
            .expect("Failed to write to file"); //see below
        
        let r = index.update(&cfg);
        assert!(r.is_ok());
        assert!(index.files.contains_key(std::path::Path::new("file7.md")));
    }

    const FILE7: &str ="---
    title:  'File 7'
    keywords: [example7]
    followups: [file1.md]
    lang: de
    ...

    # Bla bla

    Lorem [ipsum](file2.md)

    [//]: # (Links)
    ";
    
    #[test]
    fn test_index_get_zettel_none() {
        use std::collections::BTreeMap;
        use std::time::SystemTime;
        use std::path::PathBuf;
        let index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let nz = index.get_zettel(PathBuf::from("non_existing_zettel.md"));
        assert!(nz.is_none());
    }
    
    #[test]
    fn test_index_save_non_existing_file() {
        let tmp_dir = tempdir().expect("Failed to create temp directory");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config");
        let cfg_file = config_dir.join("libzettels.cfg.yaml");
        let mut cfg = Config::from_file(cfg_file)
            .expect("Failed to read config from file.");
        
        let index = Index::load(&cfg)
            .expect("Failed to read index from file.");
        // change the indexfile
        cfg.indexfile = config_dir.join("nonexisting_dir/foo"); //doesn't exist
        
        let result = index.save(&cfg);
        assert!(result.is_err());
        let e = result.unwrap_err();
        match e {
            Error::Io(inner) => {
                match inner.kind() {
                    std::io::ErrorKind::NotFound => {
                        assert!(inner.to_string().contains("No such file or directory"));
                    },
                    _ => panic!("Expected a NotFound error, got: {:#?}", inner)
                }
            },
            _ => panic!("Expected a Io error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_from_yaml_unkown_field() {
        let yaml = "---
        files:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        timestamp:
            secs_since_epoc: 1543078763
            nanos_since_epoch: 449322318";
        
        let index = Index::from_yaml(yaml);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("unknown field"));
                assert!(message.contains("expected `secs_since_epoch`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_from_yaml_missing_field() {
        let yaml = "---
        fies:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318";
        
        let index = Index::from_yaml(yaml);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("missing field `files`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_from_yaml_duplicate_field() {
        let yaml = "---
        files:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318";
        
        let index = Index::from_yaml(yaml);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("duplicate field `timestamp`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_from_yaml_invalid_type() {
        let yaml = "---
        files: foo
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318";
        
        let index = Index::from_yaml(yaml);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("invalid type"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_from_file_non_existing_file() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("foo"); //doesn't exist
        
        let index = Index::from_file(indexfile);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Io(inner) => {
                match inner.kind() {
                    std::io::ErrorKind::NotFound => {
                        assert!(inner.to_string().contains("No such file or \
                                                            directory"));
                    },
                    _ => panic!("Expected a NotFound error, got: {:#?}", inner)
                }
            },
            _ => panic!("Expected a Io error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_from_file_missing_field() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("index.yaml");

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---    
        file:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318";
        writeln!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::from_file(indexfile);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("missing field `files`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_from_file_duplicate_field() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("index.yaml");

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
            
        let erronous_index = "---
        files:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        files: foo
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318";
            write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::from_file(indexfile);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("duplicate field `files`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_from_file_invalid_type() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("index.yaml");

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
        files: foo
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 449322318";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::from_file(indexfile);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("invalid type"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_from_file_unknown_field() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("index.yaml");

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
        files:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        timestamp:
            secs_since_epoc: 1543078763
            nanos_since_epoch: 449322318";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::from_file(indexfile);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("unknown field"));
                assert!(message.contains("expected `secs_since_epoch`"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }

    #[test]
    fn test_index_from_file_invalid_value() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("index.yaml");

        let mut f = std::fs::File::create(&indexfile)
            .expect("Failed to create index file");
        
        let erronous_index = "---
        files:
            file1.md:
                title: File 1
                followups: [file2.md]
                keywords: [example, first]
                links: [subdir/file3.md]
        timestamp:
            secs_since_epoch: 1543078763
            nanos_since_epoch: 44932231800000000";
        write!(f, "{}", erronous_index).expect("Failed to write to file");
        
        let index = Index::from_file(indexfile);
        assert!(index.is_err());
        let e = index.unwrap_err();
        match e {
            Error::Yaml(inner) => {
                let message = inner.to_string();
                assert!(message.contains("invalid value"));
                assert!(message.contains("expected u32"));
            },
            _ => panic!("Expected a Yaml error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_to_file_non_existing_path() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir).expect("Failed to generate examples");
        let config_dir = dir.join("examples/config/");
        let indexfile = config_dir.join("nonexisting_dir/foo"); //doesn't exist
        let index = Index::empty();
        let result = index.to_file(indexfile);
        assert!(result.is_err());
        let e = result.unwrap_err();
        match e {
            Error::Io(inner) => {
                match inner.kind() {
                    std::io::ErrorKind::NotFound => {
                        assert!(inner.to_string().contains("No such file or directory"));
                    },
                    _ => panic!("Expected a NotFound error, got: {:#?}", inner)
                }
            },
            _ => panic!("Expected a Io error, got: {:#?}", e),
        }
    }
    
    #[test]
    fn test_index_remove_zettel_not_contained() {
        let mut index = Index {
            files: std::collections::BTreeMap::new(),
            timestamp: std::time::SystemTime::now(),
        };
        let key = std::path::PathBuf::from("non_existing_zettel.md");
        let rz = index.remove_zettel(key);
        assert!(rz.is_none());
    }
    
    #[test]
    fn test_index_get_mut_zettel_none() {
        use std::collections::BTreeMap;
        use std::time::SystemTime;
        use std::path::PathBuf;
        let mut index = Index {
            files: BTreeMap::new(),
            timestamp: SystemTime::now(),
        };
        let nz = index.get_mut_zettel(PathBuf::from("non_existing_zettel.md"));
        assert!(nz.is_none());
    }
    
    fn sequence_test_index() -> Index {
        let mut index = Index::empty();
        let mut f1 = Zettel::new("File 1");
        let mut f2 = Zettel::new("File 2");
        let mut f3 = Zettel::new("File 3");
        let mut f4 = Zettel::new("File 4");
        let f5 = Zettel::new("File 5");
        let mut f6 = Zettel::new("File 6");
        let mut f7 = Zettel::new("File 7");
        
        f1.add_keyword("example");
        f3.add_keyword("foo");
        f4.add_keyword("bar");
        f7.add_keyword("baz");
        
        f1.add_followup("file2.md");
        f2.add_followup("file3.md");
        f2.add_followup("file4.md");
        f3.add_followup("file5.md");
        f4.add_followup("file5.md"); // double parentage for this test
        f4.add_followup("file6.md");
        f6.add_followup("file7.md");
        
        index.add_zettel("file1.md", f1);
        index.add_zettel("file2.md", f2);
        index.add_zettel("file3.md", f3);
        index.add_zettel("file4.md", f4);
        index.add_zettel("file5.md", f5);
        index.add_zettel("file6.md", f6);
        index.add_zettel("file7.md", f7);
        index
    }
    
    #[test]
    fn test_sequence_tree() {
        let index = sequence_test_index();
        let mut scope = HashSet::new();
        scope.insert(PathBuf::from("file4.md"));
        scope.insert(PathBuf::from("file6.md"));
        
        let stn = querying::sequences::sequence_tree(&index, &scope);
        assert_eq!(stn.len(), 6);
        assert!(stn.contains(&PathBuf::from("file1.md")));
        assert!(stn.contains(&PathBuf::from("file2.md")));
        assert!(stn.contains(&PathBuf::from("file4.md")));
        assert!(stn.contains(&PathBuf::from("file5.md")));
        assert!(stn.contains(&PathBuf::from("file6.md")));
        assert!(stn.contains(&PathBuf::from("file7.md")));
    }
    
    #[test]
    fn test_inspect_links() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir)
                .expect("Failed to generate examples");
        let file_path = dir.join("examples/config/index.yaml");
        let index = Index::from_file(&file_path)
                .expect("Failed to load index.");
        
        // We want the links of one Zettel: file1.md
        let mut linkers = HashSet::new();
        linkers.insert(PathBuf::from("file1.md"));
        let links = index.inspect_links(&linkers);
        
        assert_eq!(links.len(), 2);
        assert!(links.contains(&PathBuf::from("file2.md")));
        assert!(links.contains(&PathBuf::from("file3.md")));
    }
    
    #[test]
    fn test_sequence_tree_whole() {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir)
                .expect("Failed to generate examples");
        let file_path = dir.join("examples/config/index.yaml");
        let index = Index::from_file(&file_path)
                .expect("Failed to load index.");
        
        let mut scope = HashSet::new();
        scope.insert(PathBuf::from("file1.md"));
        let sequence_tree = index.sequence_tree_whole(&scope);
        assert_eq!(sequence_tree.len(), 5);
        assert!(sequence_tree.contains(&PathBuf::from("file1.md")));
        assert!(sequence_tree.contains(&PathBuf::from("file2.md")));
        assert!(sequence_tree.contains(&PathBuf::from("file3.md")));
        assert!(sequence_tree.contains(&PathBuf::from("subdir/file4.md")));
        assert!(sequence_tree.contains(&PathBuf::from("subdir/file5.md")));
    }
}
