# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.4.0] 2020.06.08

## Changed
- example config path
## Fixed
- #1: bug when calling index.update on an unchanged root directory with
  ripgrep from a working directory that contained markdown files by
  chance.

## [0.3.1] - 2020.06.04

## Changed
Updated metadata and documentation, now that repository and crate exist.

# [0.3.0] - 2020.06.04
## Added
- combi_search
- now ignoring svg files
- `tests::image_based_zettels::test_create_index_with_image`
- `tests::image_based_zettels::test_create_index_with_svg`
- `tests::image_based_zettels::test_update_index_with_image`
- `tests::image_based_zettels::test_update_index_with_svg`
- new error type: BadHeader
- Tests for edge cases in markdown links
- Support for parentheses in filenames
## Changed
- Zettel::from_yaml now emits BadHeader instead of Yaml Error
## Fixed
- A bug that made indexing fail when images were present in the root directory.

## 0.2.0 - 2020.04.30
### Added
- Module `sequences`
- `querying::search_zettel_for_keywords`
- `querying::search_index_for_keywords`
- `querying::search_zettel_title`
- `querying::search_index_for_title`
- `zettel::links_to`
- `querying::inspect_incoming`
- Index: wrapper function for 
  1. `querying::sequences::sequence_tree`
  2. `querying::sequences::sequence_tree_whole`
  3. `querying::sequences::sequences`
  4. `querying::sequences::zettels_of_sequence`
  5. `querying::search_index_for_keywords`
  6. `querying::search_index_for_title`
  7. `querying::inspect_incoming`
- `index::inspect_links_of`

### Changed
- function call `parents_of_zettel` in Index
- Updated some dependencies
### Removed
- redundant functionality `parents_of_zettel`
- Old terminology of "query", namely:
    - `querying::Query`
    - `querying::query_title`
    - `querying::query_links`
    - `querying::query_keywords`
    - `querying::query_followups`
    - `querying::apply_queries`
    - `querying::query_index_for_scope`
    - `querying::query_index`
    - `index::query`
    - `index::query_for_scope`
