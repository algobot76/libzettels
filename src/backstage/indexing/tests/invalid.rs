//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with libzettels. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------

extern crate tempfile;
use self::tempfile::tempdir;
use std::io::Write;
use std::fs::{self, File};
use super::super::*;
use examples::*;

#[test]
fn test_get_list_of_files_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("foo"); //doesn't exist
    let ignorefile = PathBuf::from(".gitignore");
    let mut gitignore = File::create(dir.join(&ignorefile))
        .expect("Failed to create .gitignore");
    writeln!(gitignore, "{}", "/.*").expect("Failed to write to file");
    let list_of_files = get_list_of_files(&rootdir, &ignorefile);
    assert!(list_of_files.is_err());
    let e = list_of_files.unwrap_err();
    match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_get_list_of_files_non_existing_ignorefile() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let ignorefile = PathBuf::from(".gitignore");
    // delete the ignorefile
    fs::remove_file(rootdir.join(&ignorefile)).expect("Failed to delete file");
    let list_of_files = get_list_of_files(&rootdir, &ignorefile);
    assert!(list_of_files.is_ok());
    let list_of_files = list_of_files.unwrap();
    // A nonexisting ignorefile doesn't cause an error. It just means nothing
    // is ignored.
    assert_eq!(list_of_files.len(), 11);
    // it should be the following
    assert!(list_of_files.contains(&PathBuf::from("file1.md")));
    assert!(list_of_files.contains(&PathBuf::from("file2.md")));
    assert!(list_of_files.contains(&PathBuf::from("file3.md")));
    assert!(list_of_files.contains(&PathBuf::from(".zettelsignore")));
    assert!(list_of_files.contains(&PathBuf::from(".hiddenfile.txt")));
    assert!(list_of_files.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(list_of_files.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(list_of_files.contains(&PathBuf::from("subdir/file4.md")));
    assert!(list_of_files.contains(&PathBuf::from("subdir/file5.md")));
    assert!(list_of_files.contains(&PathBuf::from(".hiddendir/a-file.txt")));
    assert!(list_of_files.contains(&PathBuf::from(".hiddendir/.hiddenfile2.txt")));
}

#[test]
fn test_get_list_of_files_no_ignore_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist

    let list_of_files = get_list_of_files_no_ignore(&rootdir);
    assert!(list_of_files.is_err());
    let e = list_of_files.unwrap_err();
    match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_get_changed_zettels_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    
    let ignorefile = tmp_dir.path().join("examples/Zettelkasten/.gitignore");
    // start with an empty index
    let mut index = Index::empty();

        
    // Let's go.
    let changed_zettels = get_changed_zettels(&mut index,
                                              &rootdir,
                                              &ignorefile);
    assert!(changed_zettels.is_err());
    let e = changed_zettels.unwrap_err();
    match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_create_index_grep_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    let indexingmethod = IndexingMethod::Grep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_err());
    let e = index.unwrap_err();
        match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_create_index_ripgrep_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    let indexingmethod = IndexingMethod::RipGrep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_err());
    let e = index.unwrap_err();
        match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_create_index_native_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    let indexingmethod = IndexingMethod::Native;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_err());
    let e = index.unwrap_err();
        match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_update_index_grep_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::Grep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let mut index = index.unwrap();

    // Now, let's break things.
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    let result = update_index(&mut index, &indexingmethod, &rootdir, &ignorefile);
    assert!(result.is_err());
    let e = result.unwrap_err();
        match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_update_index_ripgrep_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::RipGrep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let mut index = index.unwrap();

    // Now, let's break things.
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    let result = update_index(&mut index, &indexingmethod, &rootdir, &ignorefile);
    assert!(result.is_err());
    let e = result.unwrap_err();
        match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_update_index_native_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::Native;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let mut index = index.unwrap();

    // Now, let's break things.
    let rootdir = dir.join("foo").to_path_buf(); //doesn't exist
    let result = update_index(&mut index, &indexingmethod, &rootdir, &ignorefile);
    assert!(result.is_err());
    let e = result.unwrap_err();
        match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_normalize_path_non_existing_file() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let file    = rootdir.join("foo"); //doesn't exist
    
    let result = normalize_path(&rootdir, &file);
    assert!(result.is_err());
    let e = result.unwrap_err();
    match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_normalize_path_incompatible_paths() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Note: we point to subdir here.
    let rootdir = dir.join("examples/Zettelkasten/subdir").to_path_buf();
    let file    = rootdir.join("../file1.md");
    
    let result = normalize_path(&rootdir, &file);
    assert!(result.is_err());
    let e = result.unwrap_err();
        match e {
        Error::NormalizePath(_inner) => (),
        _ => panic!("Expected NormalizePath, found {:#?}", e),
    }
}

#[test]
fn test_normalize_link_wrong_link() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    // Memo: The mistake could be in all three parameters. In this case,
    //       it's in `link`.
    let rootdir = dir.join("examples/Zettelkasten/");
    let file    = dir.join("examples/Zettelkasten/subdir/file4.md");
    let link    = PathBuf::from("../../file1.md");
    
    let result = normalize_link(&rootdir, &file, &link);
    assert!(result.is_err());
    let e = result.unwrap_err();
    match e {
        Error::BadLink(source, wrong_link, inner) => {
            assert_eq!(source, file);
            assert!(wrong_link.ends_with(link));
            assert_eq!(inner.kind(), std::io::ErrorKind::NotFound);
            assert!(inner.to_string().contains("No such file or directory"));
        },
        _ => panic!("Expected BadLink error, found {:#?}", e),
    }
}

#[test]
fn test_normalize_link_incompatible_paths() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    // Throw a NormalizePathError when the path of one of the tree 
    // parameters points to something above the root directory.
    // To cause this, we point our root directory not to our usual root,
    // but to a subdirectory. Thus, the link goes to somewhere outside the
    // root.
    let rootdir = dir.join("examples/Zettelkasten/subdir");
    let file    = dir.join("examples/Zettelkasten/subdir/file4.md");
    let link    = PathBuf::from("../file1.md");
    let result = normalize_link(&rootdir, &file, &link);
    assert!(result.is_err());
    let e = result.unwrap_err();
    match e {
        Error::NormalizePath(_inner) => (),
        _ => panic!("Expected NormalizePath, found {:#?}", e),
    }
}

#[test]
fn test_parse_and_apply_markdown_links_missing_zettel() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = dir.join("examples/Zettelkasten/");
    let f1      = rootdir.join("file1.md");
    let f2      = rootdir.join("subdir/file4.md");
    let mut index = Index::empty();
    // We'd need two zettel stubs but we omit adding them.
    //index.add_zettel("test1.md", Zettel::new("F1"));
    //index.add_zettel("subdir/test2.md", Zettel::new("F2"));
    
    let files = vec![f1, f2];
    let indexingmethod = IndexingMethod::Grep;
    
    let result = parse_and_apply_markdown_links(&indexingmethod,
                                                &mut index,
                                                &rootdir, 
                                                files);
    assert!(result.is_err());
    let e = result.unwrap_err();
    match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::Other => {
                    assert!(inner.to_string().contains("A zettel should be there, but it isn't."));
                },
                _ => panic!("Expected a Other error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_parse_and_apply_markdown_links_non_existing_rootdir() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = dir.join("examples/Zettelkasten/");
    let f1      = rootdir.join("file1.md");
    let f2      = rootdir.join("subdir/file4.md");
    // overwrite rootdir with a non-existing path
    let rootdir = tmp_dir.path().join("foo");
    let mut index = Index::empty();
    // Two zettel stubs
    index.add_zettel("file1.md", Zettel::new("F1"));
    index.add_zettel("subdir/file4.md", Zettel::new("F2"));
    
    let files = vec![f1, f2];
    let indexingmethod = IndexingMethod::Grep;
    
    let result = parse_and_apply_markdown_links(&indexingmethod,
                                                &mut index,
                                                &rootdir, 
                                                files);
    assert!(result.is_err());
    let e = result.unwrap_err();
    match e {
        Error::Io(inner) => {
            match inner.kind() {
                std::io::ErrorKind::NotFound => {
                    assert!(inner.to_string().contains("No such file or directory"));
                },
                _ => panic!("Expected a NotFound error, got: {:#?}", inner)
            }
        },
        _ => panic!("Expected a Io error, got: {:#?}", e),
    }
}

#[test]
fn test_parse_and_apply_markdown_links_bad_link() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = dir.join("examples/Zettelkasten/");
    let f1      = rootdir.join("file1.md");
    let f2      = rootdir.join("subdir/file4.md");
    std::fs::remove_file(&f1)
        .expect("Failed to delete file.");
    // write a wrong link to test1
    let mut file = std::fs::File::create(&f1)
        .expect("Failed to open temporary file.");
    file.write(b"lorem [ipsum](foo.md)")
        .expect("Failed to write to temporary file.");
    
    let mut index = Index::empty();
    // Two zettel stubs
    index.add_zettel("file1.md", Zettel::new("F1"));
    index.add_zettel("subdir/file4.md", Zettel::new("F2"));
    
    let files = vec![f1, f2];
    let indexingmethod = IndexingMethod::Grep;
    
    let result = parse_and_apply_markdown_links(&indexingmethod,
                                                &mut index,
                                                &rootdir, 
                                                files);
    assert!(result.is_err());
    let e = result.unwrap_err();
    match e {
        Error::BadLink(source, wrong_link, inner) => {
            assert_eq!(source, rootdir.join("file1.md"));
            assert!(wrong_link.ends_with("foo.md"));
            assert_eq!(inner.kind(), std::io::ErrorKind::NotFound);
            assert!(inner.to_string().contains("No such file or directory"));
        },
        _ => panic!("Expected BadLink error, found {:#?}", e),
    }
}

// add_textfiles_only() no test. This private function get sanitized data from
// the calling function (create_index or update_index). We could add some 
// normalize_path calls inside it, but those would be redundant, since the
// calling functions do this right before calling add_textfiles_only.
// So anything that would cause this to cause an error would cause an error
// on the calling function, beforehand.